<?php

use App\Http\Controllers\Api\HotelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Fetch all active hotels data
Route::get('hotels', [HotelController::class,'getAllHotelData']);

// Fetch particular hotel data
Route::get('hotel/{hotel_id}', [HotelController::class,'getHotelDataById']);

// Add a new hotel review
Route::post('save-hotel-review', [HotelController::class,'storeHotelReviewData']);

// Update hotel review
Route::put('update-hotel-review/{review_id}', [HotelController::class,'updateHotelReviewData']);

// Delete hotel review
Route::delete('review/{review_id}', [HotelController::class,'deleteHotelReview']);
