<?php

namespace Tests\Feature;

use App\Models\Hotel;
use Tests\TestCase;

class HotelTest extends TestCase
{
    /**
     * A feature test to get active hotel data based on hotel ID
     *
     * @return void
     */
    public function test_get_active_hotel_by_id(): void
    {
        $hotel_id = Hotel::query()->where('active', 1)->get()->random()->id;
        $this->get('/api/hotel/' . $hotel_id)
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'code',
                    'message',
                    'data' => [
                        'id',
                        'name',
                        'star',
                        'review' => [
                            '*' => [
                                "id",
                                "title",
                                "description",
                                "author",
                                "create_at",
                                "update_at"
                            ],
                        ]
                    ],
                ]
            );
    }

    /**
     * A feature test to get all active hotel data
     *
     * @return void
     */
    public function test_get_all_active_hotels(): void
    {
        $this->get('/api/hotels')
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'code',
                    'message',
                    'data' =>  [
                        '*' => [
                            "id",
                            "name",
                            "address",
                            "star",
                            "create_at",
                            "update_at",
                            "active",
                            "review" => [
                                '*' => [
                                    "id",
                                    "title",
                                    "description",
                                    "author",
                                    "create_at",
                                    "update_at"
                                ],
                            ],
                        ],
                    ],
                ]
            );
    }

    /**
     * A feature test to get inactive hotel data based on hotel ID
     *
     * @return void
     */
    public function test_get_inactive_hotel_by_id(): void
    {
        $hotel_id = Hotel::query()->where('active', 0)->get()->random()->id;
        $this->get('/api/hotel/' . $hotel_id)
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'code',
                    'message',
                ]
            );
    }

}
