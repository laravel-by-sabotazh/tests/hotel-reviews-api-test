<?php

namespace Tests\Feature;

use App\Models\Hotel;
use App\Models\Review;
use App\Models\User;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class ReviewTest extends TestCase
{
    /**
     * A feature test to add a new review
     *
     * @return void
     */
    public function test_for_add_hotel_review(): void
    {
        $user = User::query()->create([
            'name' => rand(),
            'email' => rand() . '.abc@xyz.com',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        //$user = User::create($userData);
        $hotel = Hotel::query()->create([
            'name' => rand(),
            'star' => 2,
            'address' => 'Opposite Town Hall, Nr. Nikolay II & IV, Ashram Rd, OldBridge, Ahmedabad, Gujarat 380006',
            'active' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $payload = [
            "hotel_id" => $hotel->pluck('id')[0],
            "user_id" => $user->pluck('id')[0],
            "review_title" => "test",
            "review_data" => "test description"
        ];

        $this->json('POST', 'api/save-hotel-review', $payload)
            ->assertStatus(200)
            ->assertJson([
                'code' => '200',
                'message' => 'Hotel Review saved.',
            ]);
    }

    /**
     * A feature test to update review based on review id
     *
     * @return void
     */
    public function test_for_update_hotel_review(): void
    {
        $user = User::query()->create([
            'name' => rand(),
            'email' => rand() . '.abc@xyz.com',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        //$user = User::create($userData);
        $hotel = Hotel::query()->create([
            'name' => rand(),
            'star' => 2,
            'address' => 'Opposite Town Hall, Nr. Nikolay II & IV, Ashram Rd, OldBridge, Ahmedabad, Gujarat 380006',
            'active' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $hotelReview = Review::query()->create([
            'title' => 'Good Hotel HollywoodInn',
            'description' => 'HollywoodInn is a very nice hotel.',
            'user_id' => $user->pluck('id')[0],
            'hotel_id' => $hotel->pluck('id')[0],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $payload = [
            "hotel_id" => $hotel->pluck('id')[0],
            "user_id" => $user->pluck('id')[0],
            "review_title" => "test",
            "review_data" => "test description"
        ];

        $this->json('PUT', 'api/update-hotel-review/' . $hotelReview->pluck('id')[0], $payload)
            ->assertStatus(200)
            ->assertJson([
                'code' => '200',
                'message' => 'Hotel Review updated.',
            ]);
    }

    /**
     * A feature test to delete hotel review data
     *
     * @return void
     */
    public function test_for_delete_hotel_review(): void
    {
        $user = User::query()->create([
            'name' => rand(),
            'email' => rand() . '.abc@xyz.com',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $hotel = Hotel::query()->create([
            'name' => rand(),
            'star' => 2,
            'address' => 'Opposite Town Hall, Nr. Nikolay II & IV, Ashram Rd, OldBridge, Ahmedabad, Gujarat 380006',
            'active' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $hotelReview = Review::query()->create([
            'title' => 'Good Hotel HollywoodInn',
            'description' => 'HollywoodInn is a very nice hotel.',
            'user_id' => $user->pluck('id')[0],
            'hotel_id' => $hotel->pluck('id')[0],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $this->json('DELETE', 'api/review/' . $hotelReview->pluck('id')[0])
            ->assertStatus(200)
            ->assertJson([
                'code' => '200',
                'message' => 'Hotel Review deleted successfully.',
            ]);
    }

    /**
     * A feature test to store new review required data
     *
     * @return void
     */
    public function test_for_add_hotel_review_required_fields(): void
    {
        $this->json('POST', 'api/save-hotel-review')
            ->assertStatus(200)
            ->assertJson([
                'code' => '401',
                'message' => 'The hotel id field is required,The user id field is required,The review title field is required,The review data field is required'
            ]);
    }

    /**
     * A feature test to update review required data
     *
     * @return void
     */
    public function test_for_update_hotel_review_required_fields(): void
    {
        $user = User::query()->create([
            'name' => rand(),
            'email' => rand() . '.abc@xyz.com',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        //$user = User::create($userData);
        $hotel = Hotel::query()->create([
            'name' => rand(),
            'star' => 2,
            'address' => 'Opposite Town Hall, Nr. Nikolay II & IV, Ashram Rd, OldBridge, Ahmedabad, Gujarat 380006',
            'active' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        //$hotel = User::create($hotelData);
        $hotelReview = Review::query()->create([
            'title' => 'Good Hotel HollywoodInn',
            'description' => 'HollywoodInn is a very nice hotel.',
            'user_id' => $user->pluck('id')[0],
            'hotel_id' => $hotel->pluck('id')[0],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $this->json('PUT', 'api/update-hotel-review/' . $hotelReview->pluck('id')[0])
            ->assertStatus(200)
            ->assertJson([
                'code' => '401',
                'message' => 'The hotel id field is required,The user id field is required,The review title field is required,The review data field is required'
         ]);
    }

    /**
     * A feature test to update reviews that do not exist
     *
     * @return void
     */
    public function test_for_update_hotel_review_that_not_exist(): void
    {
        //review id that not exist in database
        $reviewId = rand(100000, 999999);
        $payload = [
            "hotel_id" => rand(100, 999),
            "user_id" => rand(100, 999),
            "review_title" => "test",
            "review_data" => "test description"
        ];
        $this->json('PUT', 'api/update-hotel-review/' . $reviewId, $payload)
            ->assertStatus(200)
            ->assertJson([
                'code' => '401',
                'message' => 'Invalid Hotel Id or User Id or Review Id',
            ]);
    }

    /**
     * A feature test to delete a review that does not exist
     *
     * @return void
     */
    public function test_for_delete_review_that_not_exist(): void
    {
        //review id that not exist in database
        $reviewId = rand(100000, 999999);

        $this->json('DELETE', 'api/review/' . $reviewId)
            ->assertStatus(200)
            ->assertJson([
                'code' => '401',
                'message' => 'Review not found, Please try again',
            ]);
    }
}
