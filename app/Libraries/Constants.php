<?php

// Error code list
const SUCCESS = '200';   // Success
const FAILED = '100';   // Failed
const VALIDATIONERROR = '401';   // Validation
const UNAUTHORIZED = '101';  // Unauthorized User
const NOTFOUND = '400'; // Not Found

const HOTELDATANOTFOUNDMSG = 'Hotel Data Not found';
const HOTELDATAMSG = 'Hotel Data';
const HOTELREVIEWSAVEDMSG = 'Hotel Review saved.';
const HOTELREVIEWDELETEDMSG = 'Hotel Review deleted successfully.';
const HOTELREVIEWUPDATEDMSG = 'Hotel Review updated.';

const supplierArray = [
    1 => 'Own',
    2 => 'HotelBeds',
    3 => 'SunHotels'
];
